# Pre-requisites:
# Install the mysql-connector-python driver
# `pip install mysql-connector-python`
# Run the docker command to start a test SQL server
# `docker run --name my-mysql -e MYSQL_ROOT_PASSWORD=secret -d -p 3306:3306 mysql:latest`

import mysql.connector


# ref: python_sql_rule-hardcoded-sql-expression
def setup_db_env():
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        password="secret"
    )
    db_name = "test_py"
    table_name = "customers"
    cursor = db.cursor()
    cursor.execute("CREATE DATABASE IF NOT EXISTS %s" % db_name)
    cursor.execute("USE test_py")
    cursor.execute(
        "CREATE TABLE IF NOT EXISTS " + table_name + " (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), address VARCHAR(255))")
    cursor.execute("SELECT * FROM customers")
    result = cursor.fetchall()
    if len(result) == 0:
        sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
        val = [
            ('Peter', 'Lowstreet 4'),
            ('Amy', 'Apple st 652'),
            ('Hannah', 'Mountain 21'),
            ('Michael', 'Valley 345'),
            ('Sandy', 'Ocean blvd 2')
        ]
        cursor.executemany(sql, val)
        db.commit()
        db.close()


def mysql_connector(identifier, value):
    db_configurations = {
        "user": 'root', "password": 'secret', "host": 'localhost', "database": 'test_py'
    }

    with mysql.connector.connect(**db_configurations) as connction:
        with connction.cursor() as cursor:
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute("SELECT * FROM customers WHERE id = '%s'" % identifier)
            print("1: ", cursor.fetchall())
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute('INSERT INTO customers (name, address) VALUES("%s", "%s")' % value)
            connction.commit()
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute("SELECT * FROM customers WHERE id = '%s'" % identifier)
            print("1: ", cursor.fetchall())
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute('INSERT INTO customers (name, address) VALUES("%s", "%s")' % value)
            connction.commit()
            last_row_id = str(cursor.lastrowid)
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute("UPDATE customers SET name = 'Sherlock' WHERE id = '%s'" % last_row_id)
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute("SELECT * FROM customers WHERE id = '" + last_row_id + "'")
            print("2: ", cursor.fetchall())
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute("DELETE FROM customers WHERE id = '%s'" % last_row_id)
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute("SELECT * FROM customers WHERE id = '{}'".format(identifier))
            print("3: ", cursor.fetchall())
            query_one = "SELECT * FROM customers "
            query_one = query_one + "WHERE id = '%s'" % identifier
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute(query_one)
            print("4: ", cursor.fetchall())
            query_two = "SELECT * FROM customers WHERE id = '%s'" % identifier
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute(query_two)
            print("5: ", cursor.fetchall())
            query_three = "SELECT * FROM customers WHERE id = '{}'".format(identifier)
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute(query_three)
            print("6: ", cursor.fetchall())
            query_four = f"SELECT * FROM customers WHERE id = '{identifier}'"
            # ruleid: python_sql_rule-hardcoded-sql-expression
            cursor.execute(query_four)
            print("7: ", cursor.fetchall())

            # ok: python_sql_rule-hardcoded-sql-expression
            cursor.execute("INSERT INTO customers (name, address) VALUES(%s, %s)", value)
            connction.commit()
            last_row_id = str(cursor.lastrowid)
            # ok: python_sql_rule-hardcoded-sql-expression
            cursor.execute("SELECT * FROM customers WHERE id = %s", (last_row_id,))
            print("8: ", cursor.fetchall())
            # ok: python_sql_rule-hardcoded-sql-expression
            cursor.execute("UPDATE customers SET name = 'Sherlock' WHERE id = %s", (last_row_id,))
            # ok: python_sql_rule-hardcoded-sql-expression
            cursor.execute("SELECT * FROM customers WHERE id = %s", (last_row_id,))
            print("9: ", cursor.fetchall())
            # ok: python_sql_rule-hardcoded-sql-expression
            cursor.execute("DELETE FROM customers WHERE id = %s", (last_row_id,))


if __name__ == "__main__":
    setup_db_env()
    mysql_connector(1, ("Enola", "Baker Street"))
