# python-web-apps

Run app:
(provide 2 args - first argument is mostly appended to ls command or is echoed. Second command is used directly or echoed.)

Example usage:

```
python3 SubprocessPopenShellTrue.py -la pwd
```
Rule Link : https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/exec/rule-subprocess-popen-shell-true.yml?ref_type=heads
